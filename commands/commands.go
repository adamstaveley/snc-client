package commands

import (
	"encoding/json"
	// "fmt"

	"bitbucket.org/adamstaveley/snc-client/common"
	"bitbucket.org/adamstaveley/snc-client/wallet"
	zmq "github.com/pebbe/zmq4"
)

var endpoints = [3]string{"commands", "locations", "tariffs"}

// Register S&C Credentials with message broker
func Register(soc *zmq.Socket, key wallet.Key, flags common.Flags) {
	// create the message content, minus signature
	message := common.Request{}
	// set headers
	message.Headers.RequestID = common.GenerateUUIDv4()
	message.Headers.CorrelationID = common.GenerateUUIDv4()
	message.Headers.Module = "snc-credentials"
	message.Headers.Method = "create"
	// create and set S&C Credentials message body
	body := common.SncCredentials{}
	body.Modules = make([]common.ModuleData, 1)
	body.Modules[0].Version = "2.2"
	body.Modules[0].Endpoints = make([]common.Endpoints, len(endpoints))
	for i, endpoint := range endpoints {
		body.Modules[0].Endpoints[i].Identifier = endpoint
		body.Modules[0].Endpoints[i].Role = flags.Register.Role
	}
	body.BusinessDetails.Name = flags.Register.BusinessDetailsName
	body.PartyAddress = key.Address
	body.PartyID = flags.Register.PartyID
	body.CountryCode = flags.Register.CountryCode
	message.Body = body
	// create and append signature of JSON request object
	jsonMsg, err := json.Marshal(message)
	if err != nil {
		panic(err)
	}
	message.Signature = wallet.SignMessage(key.PrivateKey, jsonMsg)
	// send final message
	jsonMsg, err = json.Marshal(message)
	if err != nil {
		panic(err)
	}
	soc.SendBytes(jsonMsg, zmq.DONTWAIT)
}

// DiscoverParties finds all parties connected to the message broker
func DiscoverParties(soc *zmq.Socket, key wallet.Key) {
	message := common.Request{}
	message.Headers.RequestID = common.GenerateUUIDv4()
	message.Headers.CorrelationID = common.GenerateUUIDv4()
	message.Headers.Module = "snc-credentials"
	message.Headers.Method = "read_all"
	
	jsonMsg, err := json.Marshal(message)
	if err != nil {
		panic(err)
	}
	message.Signature = wallet.SignMessage(key.PrivateKey, jsonMsg)

	jsonMsg, err = json.Marshal(message)
	if err != nil {
		panic(err)
	}
	soc.SendBytes(jsonMsg, zmq.DONTWAIT)
}

// RequestLocations from a specific charge point operator
func RequestLocations(soc *zmq.Socket, key wallet.Key, cpo string) {
	message := common.Request{}
	message.Headers.RequestID = common.GenerateUUIDv4()
	message.Headers.CorrelationID = common.GenerateUUIDv4()
	message.Headers.Module = "locations"
	message.Headers.Method = "GET List"
	message.Headers.To = cpo

	jsonMsg, err := json.Marshal(message)
	if err != nil {
		panic(err)
	}
	message.Signature = wallet.SignMessage(key.PrivateKey, jsonMsg)

	jsonMsg, err = json.Marshal(message)
	if err != nil {
		panic(err)
	}
	soc.SendBytes(jsonMsg, zmq.DONTWAIT)
}

// RequestTariffs from a specific charge point operator
func RequestTariffs(soc *zmq.Socket, key wallet.Key, cpo string) {
	message := common.Request{}
	message.Headers.RequestID = common.GenerateUUIDv4()
	message.Headers.CorrelationID = common.GenerateUUIDv4()
	message.Headers.Module = "tariffs"
	message.Headers.Method = "GET"
	message.Headers.To = cpo

	jsonMsg, err := json.Marshal(message)
	if err != nil {
		panic(err)
	}
	message.Signature = wallet.SignMessage(key.PrivateKey, jsonMsg)

	jsonMsg, err = json.Marshal(message)
	if err != nil {
		panic(err)
	}
	soc.SendBytes(jsonMsg, zmq.DONTWAIT)
}
