package common

/*
	OCPI classes from v2.2 [RC1], available at ../docs/OCPI-2.2-RC1.pdf
*/

// DisplayText OCPI class
type DisplayText struct {
	Language string `json:"language"`
	Text     string `json:"text"`
}

// Image OCPI class
type Image struct {
	URL       string `json:"url"`
	Thumbnail string `json:"thumbnail,omitempty"`
	Category  string `json:"category"`
	Type      string `json:"type"`
	Width     int    `json:"width,omitempty"`
	Height    int    `json:"height,omitempty"`
}

// GeoLocation OCPI class
type GeoLocation struct {
	Latitude  string `json:"latitude"`
	Longitude string `json:"longitude"`
}

// BusinessDetails OCPI class
type BusinessDetails struct {
	Name    string `json:"name"`
	Website string `json:"website,omitempty"`
	Logo    *Image `json:"logo,omitempty"`
}

// ExceptionalPeriod OCPI class
type ExceptionalPeriod struct {
	PeriodBegin string `json:"period_begin"`
	PeriodEnd   string `json:"period_end"`
}

// Hours OCPI class
type Hours struct {
	TwentyFourSeven bool `json:"twentyfourseven"`

	RegularHours []struct {
		WeekDay int `json:"weekday"`
		ExceptionalPeriod
	} `json:"regular_hours,omitempty"`

	ExceptionalOpenings ExceptionalPeriod `json:"exceptional_openings,omitempty"`
	ExceptionalClosings ExceptionalPeriod `json:"exceptional_closings,omitempty"`
}

// EnergyMix OCPI class
type EnergyMix struct {
	IsGreenEnergy bool `json:"is_green_energy"`

	EnergySources []struct {
		Source     string `json:"source"`
		Percentage int    `json:"percentage"`
	} `json:"energy_sources,omitempty"`

	EnvironImpact []struct {
		Category string `json:"category"`
		Amount   int    `json:"amount"`
	} `json:"environ_impact,omitempty"`

	SupplierName      string `json:"supplier_name,omitempty"`
	EnergyProductName string `json:"energy_product_name,omitempty"`
}

// Location OCPI class
type Location struct {
	ID          string      `json:"id"`
	Type        string      `json:"type"`
	Name        string      `json:"name"`
	Address     string      `json:"address"`
	City        string      `json:"city"`
	PostalCode  string      `json:"postal_code"`
	State       string      `json:"state,omitempty"`
	Country     string      `json:"country"`
	Coordinates GeoLocation `json:"coordinates"`

	RelatedLocations []struct {
		GeoLocation
		Name DisplayText `json:"name,omitempty"`
	} `json:"related_locations,omitempty"`

	Evses struct {
		UID    string `json:"uid"`
		EvseID string `json:"evse_id,omitempty"`
		Status string `json:"status"`

		StatusSchedule []struct {
			PeriodBegin string `json:"period_begin"`
			PeriodEnd   string `json:"period_end,omitempty"`
			Status      string `json:"status"`
		} `json:"status_schedule,omitempty"`

		Capabilities []string `json:"capabilities,omitempty"`

		Connectors []struct {
			ID                 string   `json:"id"`
			Standard           string   `json:"standard"`
			Format             string   `json:"format"`
			PowerType          string   `json:"power_type"`
			Voltage            int      `json:"voltage"`
			Amperage           int      `json:"amperage"`
			MaxElectricPower   int      `json:"max_electric_power,omitempty"`
			TariffID           string   `json:"tariff_id,omitempty"`  // support 2.1.1
			TariffIDs          []string `json:"tariff_ids,omitempty"` // support 2.2
			TermsAndConditions string   `json:"terms_and_conditions,omitempty"`
			LastUpdated        string   `json:"last_updated"`
		} `json:"connectors"`

		FloorLevel          string        `json:"floor_level,omitempty"`
		Coordinates         GeoLocation   `json:"coordinates,omitempty"`
		PhysicalReference   string        `json:"physical_reference,omitempty"`
		Directions          []DisplayText `json:"directions,omitempty"`
		ParkingRestrictions []string      `json:"parking_restrictions,omitempty"`
		Images              []Image       `json:"images,omitempty"`
		LastUpdated         string        `json:"last_updated"`
	} `json:"evses,omitempty"`

	Directions         []DisplayText   `json:"directions,omitempty"`
	Operator           BusinessDetails `json:"operator,omitempty"`
	SubOperator        BusinessDetails `json:"suboperator,omitempty"`
	Owner              BusinessDetails `json:"owner,omitempty"`
	Facilities         []string        `json:"facilities,omitempty"`
	TimeZone           string          `json:"time_zone,omitempty"`
	OpeningTimes       Hours           `json:"opening_times,omitempty"`
	ChargingWhenClosed bool            `json:"charging_when_closed,omitempty"`
	Images             []Image         `json:"images,omitempty"`
	EnergyMix          EnergyMix       `json:"energy_mix,omitempty"`
	LastUpdated        string          `json:"last_updated"`
}
