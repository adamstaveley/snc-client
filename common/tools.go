package common

import (
	"github.com/google/uuid"
)

// GenerateUUIDv4 for request_id and correlation_id
func GenerateUUIDv4() string {
	uuid, _ := uuid.NewRandom()
	return uuid.String()
}
