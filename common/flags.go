package common

// Flags describe all command line flags for each module
type Flags struct {
	Register struct {
		PartyID string
		BusinessDetailsName string
		CountryCode string
		Role string
	}
	General struct {
		ChargePointOperator string
	}
}
