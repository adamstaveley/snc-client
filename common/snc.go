package common

// Request is the data format used in requests, based on OCPI, minus Signature
type Request struct {
	Signature string `json:"signature,omitempty"`
	Headers   struct {
		RequestID     string `json:"request_id"`
		CorrelationID string `json:"correlation_id"`
		Module        string `json:"module"`
		Method        string `json:"method"`
		To            string `json:"to,omitempty"`
	} `json:"headers"`
	Body interface{} `json:"body,omitempty"`
}

// Response is the data format used in responses, based on OCPI
type Response struct {
	Headers struct {
		RequestID     string `json:"request_id,omitempty"`
		CorrelationID string `json:"correlation_id,omitempty"`
		Module        string `json:"module,omitempty"`
		Method        string `json:"method,omitempty"`
		To            string `json:"to,omitempty"`
		From          string `json:"from,omitempty"`
	} `json:"headers"`
	Body struct {
		StatusCode    int         `json:"status_code"`
		StatusMessage string      `json:"status_message,omitempty"`
		Data          interface{} `json:"data,omitempty"`
		Timestamp     string      `json:"timestamp"`
	} `json:"body"`
}

// Endpoints custom OCPI class
type Endpoints struct {
	Identifier string `json:"identifier"`
	Role       string `json:"role"`
}

// ModuleData custom OCPI class
type ModuleData struct {
	Version   string      `json:"version"`
	Endpoints []Endpoints `json:"endpoints"`
}

// SncCredentials custom OCPI class
type SncCredentials struct {
	Modules         []ModuleData    `json:"modules"`
	BusinessDetails BusinessDetails `json:"business_details"`
	PartyAddress    string          `json:"party_address"`
	PartyID         string          `json:"party_id"`
	CountryCode     string          `json:"country_code"`
}
