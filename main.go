package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"time"

	"github.com/joho/godotenv"
	zmq "github.com/pebbe/zmq4"

	"bitbucket.org/adamstaveley/snc-client/commands"
	"bitbucket.org/adamstaveley/snc-client/common"
	"bitbucket.org/adamstaveley/snc-client/wallet"
)

var flags = common.Flags{}

func init() {
	flag.StringVar(&flags.Register.PartyID, "party-id", "", "CPO or eMSP ID of this party. (following the 15118 ISO standard) [3]")
	flag.StringVar(&flags.Register.BusinessDetailsName, "business-name", "", "	Name of the operator.")
	flag.StringVar(&flags.Register.CountryCode, "country", "", "Country code of the country this party is operating in [2]")
	flag.StringVar(&flags.Register.Role, "role", "", "Type of Role: CPO or EMSP")
	flag.StringVar(&flags.General.ChargePointOperator, "cpo", "", "The party (ethereum) address of the Charge Point Operator")
}

func getKeypair() (keypair wallet.Key, err error) {
	if privateKey := os.Getenv("PRIV_KEY"); privateKey != "" {
		keypair, err = wallet.FromPrivateKey(privateKey)
		// fmt.Printf("Identity: %v\n", keypair.Address)
	} else {
		keypair, err = wallet.Generate()
		fmt.Printf("Identity: %v\nPrivate key: %v\n", keypair.Address, keypair.PrivateKey)
	}
	return keypair, err
}

func prepareSocket(identity string) (soc *zmq.Socket, err error) {
	soc, err = zmq.NewSocket(zmq.DEALER)
	if err != nil {
		return soc, err
	}
	soc.SetIdentity(identity)
	brokerAddress := os.Getenv("BROKER_ADDR")
	if brokerAddress == "" {
		brokerAddress = "tcp://0.0.0.0:3000"
	}
	soc.Connect(brokerAddress)
	// fmt.Println("Connected to", brokerAddress)
	return soc, err
}

func readMessages(soc *zmq.Socket) {
	for {
		msg, _ := soc.RecvBytes(0)
		if len(msg) > 0 {
			// fmt.Printf("Received message: %v\n", msg)
			var response common.Response
			err := json.Unmarshal(msg, &response)
			if err != nil {
				panic(err)
			}
			body, err := json.MarshalIndent(response.Body, "", "  ")
			if err != nil {
				panic(err)
			}
			fmt.Println(string(body))
			break
		}
		time.Sleep(time.Second)
	}
}

func main() {
	// load .env file - fail silently
	godotenv.Load()
	// parse command line arguments and flags specified by user
	flag.Parse()
	if len(flag.Args()) == 0 {
		panic("no command specified")
	}
	// generate a keypair from private key or from random
	keypair, err := getKeypair()
	if err != nil {
		panic(err)
	}
	// create a ZMQ socket and connect to S&C message broker
	soc, err := prepareSocket(keypair.Address)
	if err != nil {
		panic(err)
	}
	// handle command line arguments 
	switch flag.Arg(0) {
	case "register":
		commands.Register(soc, keypair, flags)
	case "parties":
		commands.DiscoverParties(soc, keypair)
	case "locations":
		commands.RequestLocations(soc, keypair, flags.General.ChargePointOperator)
	case "tariffs":
		commands.RequestTariffs(soc, keypair, flags.General.ChargePointOperator)
	default:
		panic("unknown command")
	}
	// read messages until response received
	readMessages(soc)
}
