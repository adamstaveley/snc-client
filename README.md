# Share&Charge Client

A basic command line interface for the Share&Charge network.

Current features:

- Connect to any message broker
- Use or generate a keypair
- Register with a message broker
- Find parties connected to a broker
- Find locations owned by a CPO
- Find tariffs set by a CPO

Todo:

- Add command to start a charging session
- Add command to stop a charging session
- Add command to request a charge detail record for a session
- Add command to pay the amount of a given charge detail record

## Install

```
go get bitbucket.org/adamstaveley/snc-client
```

After you have downloaded the package, install as follows:
```
cd snc-client
go build
```

This will create an `snc-client` binary in the root directory of the project.

## Usage

By default, the client will generate a private key for you and attempt to connect to a broker on the localhost.

Environment variables can be set to configure the client to use a particular Ethereum keypair and S&C message broker.
These variables can be set in an `.env` file as follows:
```
PRIVATE_KEY=9a95f237c13c637cebaa63ec234bd46ced80b6138609d0cf8a66e69a8ea1ef05
BROKER_ADDR=tcp://node39332-test-msg-broker.hidora.com:11085
```

### Running the client

```
./snc-client [flags] [command]
```

For example, to register a party:
```
./snc-client --role EMSP --country DE --party-id ABC --name "Test Service Provider GmbH" register
```

### Commands and flags

Note: flags must come before the command.

- `register` - Register your client with the S&C message broker (note: do this before anything else)
    - `--role` - The role of the party: EMSP or CPO
    - `--country` - The country operated in
    - `--party-id` - The registered party id
    - `--name` - The name of the business
- `parties` - List all parties registered with the broker
- `locations` - List all locations provided by a CPO
    - `--cpo` - The party (Etheruem) address of the CPO
- `tariffs` - List all tariffs provided by a CPO
    - `--cpo` - The party (Ethereum) address of the CPO

### Processing data

`snc-client` makes no assumptions about how responses should be formatted. Instead, additional tools such as [`jq`](https://stedolan.github.io/jq/) can be
leveraged to parse the JSON responses in whichever preferred way.

#### Examples

1. List connected party names and addresses

```bash
$ ./snc-client parties | jq '.data[] | { name: .business_details.name, addr: .party_address }'
{
  "name": "My Test MSP",
  "addr": "0x45DA89aEc9c54E4d5F463ac678200FdDf64D2fF3"
}
{
  "name": "My Test CPO",
  "addr": "0x975A8fBC9ee17151492bf324B4DC9A50a41Ac9a6"
}
```

2. List information about locations

```bash
$ ./snc-client --cpo=0x975A8fBC9ee17151492bf324B4DC9A50a41Ac9a6 locations | jq '.data[] | { id: .id, address: .address, city: .city, evses: [.evses[].uid], tariffs: [.evses[].connectors[].tariff_id] }'
{
  "id": "PH03482",
  "address": "Rüttenscheider Str. 120",
  "city": "Essen",
  "evses": [
    "BB-5983-3"
  ],
  "tariffs": [
    "xxx-000"
  ]
}
```

3. List a specific tariff by its ID

```bash
$ ./snc-client --cpo=0x27C43eD4149d87784bca7F2D0F2791A8f414F034 tariffs | jq '.data[] | select(.id == "xxx-000")'
{
  "currency": "EUR",
  "elements": [
    {
      "price_components": [
        {
          "price": 1,
          "step_size": 1,
          "type": "FLAT"
        }
      ]
    }
  ],
  "id": "xxx-000"
}
```

### FAQ

1. Program hangs, no response received

    Either:

    - There is no broker bound to the address provided (by default `tcp://0.0.0.0:3000` unless the `BROKER_ADDR` env var is specified).
    - You have not registered with the broker, meaning you cannot receive any messages.

2. `jq` parsing fails

    - Make sure to save the provided private key from your first run to the env var `PRIVATE_KEY`. The program will print a generated private key if it cannot find this variable.
    - Find documentation [here](https://stedolan.github.io/jq/tutorial/).
