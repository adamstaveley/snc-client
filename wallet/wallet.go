package wallet

import (
	"crypto/ecdsa"
	"strconv"

	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
)

// Key holds the Private Key and Address
type Key struct {
	Address    string
	PrivateKey string
}

// Generate creates a new Ethereum-compliant key-pair
func Generate() (Key, error) {
	privateKey, err := crypto.GenerateKey()
	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		panic("error casting public key to ECDSA")
	}
	return Key{
		PrivateKey: hexutil.Encode(crypto.FromECDSA(privateKey))[2:],
		Address:    crypto.PubkeyToAddress(*publicKeyECDSA).Hex()}, err
}

// FromPrivateKey generates a wallet from the given private key string
func FromPrivateKey(privKeyString string) (Key, error) {
	if privKeyString[:2] == "0x" {
		privKeyString = privKeyString[2:]
	}
	privateKey, err := crypto.HexToECDSA(privKeyString)
	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		panic("error casting public key to ECDSA")
	}
	return Key{
		PrivateKey: hexutil.Encode(crypto.FromECDSA(privateKey))[2:],
		Address:    crypto.PubkeyToAddress(*publicKeyECDSA).Hex()}, err
}

// SignMessage with private key to send to message broker
func SignMessage(privKeyString string, message []byte) string {
	privateKey, err := crypto.HexToECDSA(privKeyString)
	if err != nil {
		panic(err)
	}
	prefix := []byte("\x19Ethereum Signed Message:\n" + strconv.FormatInt(int64(len(message)), 10))
	message = append(prefix, message...)
	hash := crypto.Keccak256Hash(message)
	signature, err := crypto.Sign(hash.Bytes(), privateKey)
	if err != nil {
		panic(err)
	}
	return hexutil.Encode(signature)
}
